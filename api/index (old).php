<?php

session_start();
set_time_limit(0);

require_once ('vendor/autoload.php');
require_once ('get_json.php');
require_once ('CurlWrapper.php');

function getSchoolSemester($school, $semester) {
    if ($school == 'Samsung-WAC') {
        $semester = 'S' . $semester;
    } else {
        $semester = 'W' . $semester;
    }
    return $semester;
}

function getSchool($course) {
    if ($course == 'wac-ambition-feminine') {
        return 'webacademie';
    } else return $course;
}

$app = new \Slim\Slim();
if (!isset($_SESSION['user']["login"])) {
    $_SESSION['user']["login"] = false;
}

function getStudents($country, $town, $scolaryear, $school, $semester, $json = false) {
    $semester = getSchoolSemester($school, $semester);
    //https://intra.epitech.eu/admin/promo/list?school=webacademie&scolaryear=2015&course=webacademie&semester=W1&location=FR/LYN&format=json
    $wrapper = new CurlWrapper(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt");
    $students = json_decode($wrapper->get(
        'https://intra.epitech.eu/admin/promo/list?' .
        '&location=' . $country . '/' . $town .
        '&scolaryear=' . $scolaryear .
        '&school=' . getSchool($school) .
        '&course=' . $school .
        '&semester=' . $semester .
        '&format=json'
    ));
    foreach ($students as $student) {
        $student->netsoul = getNetsoul($student->login);
        $student->notes = getNotes($student->login);
        $student->student = getStudent($student->login);
    }
    if (!$json) {
        return $students;
    }
    return json_encode($students);
    // echo get_json(
    //     realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt",
    //     'https://intra.epitech.eu/admin/promo/list?' .
    //     '&location=' . $country . '/' . $town .
    //     '&scolaryear=' . $scolaryear .
    //     '&school=' . getSchool($school) .
    //     '&course=' . $school .
    //     '&semester=' . $semester .
    //     '&format=json',
    //     null
    // );
}

$app->get(
    '/students/:country/:town/:scolaryear/:school/:semester',
    function ($country = 'FR', $town = 'LYN', $scolaryear = 2015, $school = 'webacademie', $semester = 'W1') {
        echo getStudents($country, $town, $scolaryear, $school, $semester, true);
    }
);

function getStudent ($login, $json = false) {
    //https://intra.epitech.eu/user/odin.duclos@epitech.eu/?format=json
    $wrapper = new CurlWrapper(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt");
    $student = $wrapper->get('https://intra.epitech.eu/user/' . $login . '/?format=json');
    if (!$json) {
        return json_decode($student);
    }
    return $student;
}

$app->get(
    '/student/:login', function ($login) {
        echo getStudent($login, true);
    }
);

function sortByOrder($a, $b) {
    return $a[0] > $b[0];
}

function getNetsoul ($login, $json = false) {
    $dates = get_json(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt", 'https://intra.epitech.eu/user/' . $login . '/netsoul?format=json', null);
    $dates = json_decode($dates);
    foreach ($dates as $key => &$date) {
        array_push($date, $date[0]);
        if (isset($date[0])) {
            $date[0] = date('Y-m-d', $date[0]);
        }
        if (isset($date[1]) && $date[1] !== 0) {
            $date[1] = ($date[1] / 60) / 60;
        }
    }
    usort($dates, 'sortByOrder');
    $jours_ouvres = 0;
    $jours_semaine = 0;
    $logtime_ouvres = 0;
    $logtime_semaine = 0;
    for ($i=count($dates)-2; $i >= 0; $i--) {
        $date = $dates[$i];
        $day = date('w', $date[count($date) - 1]);
        if ($jours_semaine < 7) {
            $logtime_semaine += $date[1];
            $jours_semaine++;
        }
        if ($jours_ouvres < 5 && $day != '6' && $day != '0') {
            $logtime_ouvres += $date[1];
            $jours_ouvres++;
        }
    }
    $logtime_semaine_avg = $logtime_semaine / 7;
    $logtime_ouvres_avg = $logtime_ouvres / 5;
    if (!$json) {
        return array($logtime_semaine, $logtime_semaine_avg, $logtime_ouvres, $logtime_ouvres_avg, $dates);
    }
    else {
        return json_encode(array($logtime_semaine, $logtime_semaine_avg, $logtime_ouvres, $logtime_ouvres_avg, $dates));
    }
}

$app->get('/netsoul/:login', function ($login) {
    echo getNetsoul($login, true);
});

function getModules($country = 'FR', $town = 'LYN', $scolaryear = 2015, $school = 'webacademie', $semester = 'W1') {
    // https://intra.epitech.eu/admin/module/list?&location=FR/LYN&scolaryear=2015&school=webacademie&course=webacademie&semester=W2&format=json
    $semester = getSchoolSemester($school, $semester);
    $json_modules = json_decode(get_json(
        realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt",
        'https://intra.epitech.eu/admin/module/list?' .
        '&location=' . $country  . '/' . $town .
        '&scolaryear=' . $scolaryear .
        '&school=' . getSchool($school) .
        '&course=' . $school .
        '&semester=' . $semester .
        '&format=json',
        null
    ));
    $json_students = json_decode(get_json(
        realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt",
        'https://intra.epitech.eu/admin/promo/list?' .
        'school=' . getSchool($school) .
        '&scolaryear=' . $scolaryear .
        '&course=' . $school .
        '&semester=' . $semester .
        '&location=' . $country  . '/' . $town .
        '&format=json',
        null
    ));
    $students = [];
    foreach ($json_students as $v)
    {
        $students[$v->login]['credits'] = $v->credits;
        $students[$v->login]['title'] = $v->title;
    }
    ksort($students);
    $modules = [];
    foreach ($json_modules as $k)
    {
        $instance = $k->code . $k->codeinstance;
        $modules[$instance]['title'] = $k->title;
        $modules[$instance]['code'] = $k->code;
        $modules[$instance]['credits'] = $k->credits;
        $modules[$instance]['codeinstance'][] = $k->codeinstance;
        sort($modules[$instance]['codeinstance']);
        $data = json_decode(get_json(
            realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt",
            'https://intra.epitech.eu/module/' . $scolaryear . '/' . $k->code . '/' . $k->codeinstance . '/notes?format=json',
            null
        ));
        unset($data->columns->login);
        $modules[$instance]['projects'] = [];
        foreach ($data->columns as $key => $val)
        {
            $modules[$instance]['projects'][$key] = $val->help;
        }
        foreach ($data->listnotes as $list)
        {
            if (array_key_exists($list->login, $students))
            {
                foreach ($modules[$instance]['projects'] as $key => $val)
                {
                    if (isset($list->{$key}))
                        $modules[$instance]['students'][$list->login][$key] = $list->{$key};
                }
            }
        }
    }
    return $modules;
}

$app->get(
    '/modules/:country/:town/:scolaryear/:school/:semester',
    function ($country = 'FR', $town = 'LYN', $scolaryear = 2015, $school = 'webacademie', $semester = 'W1') {
        echo json_encode(getModules($country, $town, $scolaryear, $school, $semester));
    }
);

$app->get('/logs', function () {
    echo get_json(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt", 'https://intra.epitech.eu/pedago/log/get?format=json', null);
});

$app->get('/pp/:country/:town/:scolaryear/:semester', function ($country = 'FR', $town = 'LYN', $scolaryear = 2015, $semester = 1) {
    $query = 'https://intra.epitech.eu/module';
    $modulename = '';
    $location = '';
    if ($town == 'LYN') {
        $location = 'LYN-' . $semester . '-1';
    } else if ($town == 'PAR') {
        $location = 'PAR-' . $semester . '-1';
    }
    if ($semester == 1) {
        $modulename = 'W-ADM-050';
    }
    if ($semester == 2) {
        $modulename = 'W-ADM-150';
    }
    $query .= '/' . $scolaryear;
    $query .= '/' . $modulename;
    $query .= '/' . $location;
    $module = json_decode(get_json(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt", $query . '/?format=json', null));
    $slots = [];
    if (isset($module->activites)) {
	    foreach ($module->activites as $key => $cur_activity) {
	        $last_monday = date_create(date('Y-m-d', strtotime('last Monday')));
	        $next_sunday = date_create(date('Y-m-d', strtotime('next Sunday')));
	        $current_acti = date_create(substr($cur_activity->start, 0, 10));
	        if ($last_monday <= $current_acti && $next_sunday >= $current_acti) {
	            $query_activity = $query . '/' . $cur_activity->codeacti . '/rdv';
	            $activity = json_decode(get_json(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt", $query_activity . '/?format=json', null));
	            if ($activity->title == 'Point Pangolin') {
	                foreach ($activity->slots as $key => $value) {
	                    $slots = array_merge($slots, $value->slots);
	                }
	            }
	        }
	    }
    }
    echo json_encode($slots);
});

function getNotes ($login, $json = false) {
    $wrapper = new CurlWrapper(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt");
    $data = $wrapper->get('https://intra.epitech.eu/user/' . $login . '/notes?format=json');
    if (!$json) {
        return json_decode($data);
    }
    return $data;
}

$app->get('/notes/:login', function ($login) {
   echo getNotes($login, true);
});

$app->get('/suivis/:login', function ($login) {
    echo get_json(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt", 'https://intra.epitech.eu/user/' . $login . '/comments/list?format=json&offset=0&types[]=suivi_adm', null);
});

$app->post('/login', function () use ($app) {
    $params = json_decode($app->request->getBody());
    if (isset($params->login)) {
        $login = $params->login;
    } else {
        $login = '';
    }
    if (isset($params->password)) {
        $password = $params->password;
    } else {
        $password = '';
    }
    if (isset($params->auth)) {
        $auth = $params->auth;
    } else {
        $auth = 'classic';
    }
    $name = str_replace('@epitech.eu', '', $login);
    $name = str_replace('.', '-', $name);
    session_regenerate_id();
    $cookie = realpath('cookies') . "/php_cookie_" . $name . ".txt";
    if ($auth == 'classic') {
        require_once ('classic_connect.php');
        $http_code = classic_connect($cookie, $login, $password);
    }
    else {
        require_once ('microsoft_connect.php');
        $http_code = microsoft_connect($cookie, $login, $password);
    }
    if ($http_code == 200) {
        $_SESSION['user']['login'] = $name;
        $_SESSION['user']['password'] = md5($password);
        echo json_encode(true);
    } else {
        echo "Login ou mot de passe faux. Utilisez vos identifiants intranet.";
    }
});

$app->get('/logout', function () {
    if (isset($_SESSION['user']["login"]) && file_exists(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt")) {
        unlink(realpath('cookies') . "/php_cookie_" . $_SESSION['user']["login"] . ".txt");
    }
    session_unset();
});

$app->get('/isLogged', function () {
    if (!isset($_SESSION['user']['login']) || !isset($_SESSION['user']['password'])) {
        echo json_encode(false);
    }
    else echo json_encode(true);
});

$app->run();

?>