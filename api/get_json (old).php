<?php

function get_json($cookie, $query, $params) {
	if (isset($params['agenda']) && $params['agenda'] != 'all') {
		// start=2015-07-15&end=2015-07-22
		if ($params['agenda'] == 'last-month') {
			$end_date = date('Y-m-d', strtotime('last day of previous month'));
			$start_date = date('Y-m-d', strtotime('first day of previous month'));
		} else {
			$end_date = date('Y-m-d');
			$start_date = date('Y-m-d', strtotime(date('Y-m') . '-01'));
		}
		$query .= '&start=' . $start_date . '&end=' . $end_date;
	}
	$c = curl_init($query);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($c, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)');
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
	$page = curl_exec($c);
	curl_close($c);
	return $page;
}

?>