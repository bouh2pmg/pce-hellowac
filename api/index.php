<?php
// web/index.php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/CurlWrapper.php';
require_once __DIR__.'/utils.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();
$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'dbname' => 'hellowac',
        'user' => 'hellowac',
        'password' => 'protect.air.spandrel.mumps',
        'host' => 'wac.epitech.eu',
        'driver' => 'pdo_mysql',
    ),
));

define ('ADMIN_GROUP', 'Pangolins Responsables');

$app['debug'] = true;

$app->before(function ($request, $app) {
    $route = $request->get('_route');
    if (($route != 'POST_login' && $route != 'GET_isLogged') && $app['session']->get('user') == null) {
        return new Response('Vous n\'êtes pas authentifiés.', 401);
    }
    $user = $app['session']->get('user');
    if ($user != null) {
        $app['cookie'] = realpath('cookies') . "/php_cookie_" . $user['login'] . ".txt";
    } else {
        $app['cookie'] = realpath('cookies') . "/php_cookie_default.txt";
    }
    if (strpos($route, 'comments') !== false) {
        $admin = false;
        foreach ($user['groups'] as $key => $value) {
            if ($value->title == ADMIN_GROUP) {
                $admin = true;
            }
        }
        if (!$admin) {
            return new Response('Vous n\'avez pas les droits sur cette page.', 401);
        }
    }
});

//$app->after(function ($request, $response) {
//    $route = $request->get('_route');
////    if (strpos($route, '/students/') > 0) {
//        $handle = fopen('cache/' . $route, "w+");
//        fwrite($handle, $response);
//        fclose($handle);
////    }
//});

$app->post('/comments/add', function (Request $request) use ($app) {
    $user = $app['session']->get('user');
    $params = json_decode($request->getContent());
    $login = $params->login;
    $content = $params->content;
    $author = $params->author;
    return $app['db']->insert('comments', array('content' => $content, 'login' => $login, 'author' => $user['login']));
});

$app->post('/comments/update/content/{id}', function (Request $request, $id) use ($app) {
    $user = $app['session']->get('user');
    $params = json_decode($request->getContent());
    $content = $params->content;
    $author = $params->author;
    $stmt = $app['db']->prepare('UPDATE `hellowac`.`comments` SET `content` = :content WHERE `id` = :id');
    $stmt->bindValue("content", $content);
    $stmt->bindValue("id", $id);
    $stmt->execute();
    return $app['db']->insert('update_comments', array('login' => $user['login'], 'id_comment' => $id, 'action' => 'UPDATE_CONTENT'));
});

$app->get('/comments/update/state/{id}/{state}', function ($id, $state) use ($app) {
    $user = $app['session']->get('user');
    $stmt = $app['db']->prepare('UPDATE `hellowac`.`comments` SET `state` = :state WHERE `id` = :id');
    $stmt->bindValue("state", $state);
    $stmt->bindValue("id", $id);
    $stmt->execute();
    return $app['db']->insert('update_comments', array('login' => $user['login'], 'id_comment' => $id, 'action' => 'UPDATE_STATE'));
});

$app->get('/comments/delete/{id}', function ($id) use ($app) {
    $user = $app['session']->get('user');
    $app['db']->insert('update_comments', array('login' => $user['login'], 'id_comment' => $id, 'action' => 'DELETE'));
    return $app['db']->delete('comments', array('id' => $id));
});

$app->get('/comments/get/{login}', function ($login) use ($app) {
    $stmt = $app['db']->prepare('SELECT * FROM `comments` WHERE `login` = :login');
    $stmt->bindValue("login", $login);
    $stmt->execute();
    $entries = $stmt->fetchAll();
    for ($i=0; $i < count($entries); $i++) { 
        $stmt_updates = $app['db']->prepare('SELECT * FROM `update_comments` WHERE `id_comment` = :id');
        $stmt_updates->bindValue("id", $entries[$i]['id']);
        $stmt_updates->execute();
        $entries[$i]['updates'] = $stmt_updates->fetchAll();
    }
    return json_encode($entries);
});

$app->get('/tickets/get/{login}', function ($login) use ($app) {
    $stmt = $app['db']->prepare('SELECT * FROM `comments` WHERE `login` = :login AND `state` = 1');
    $stmt->bindValue("login", $login);
    $stmt->execute();
    $entries = $stmt->fetchAll();
    for ($i=0; $i < count($entries); $i++) { 
        $stmt_updates = $app['db']->prepare('SELECT * FROM `update_comments` WHERE `id_comment` = :id');
        $stmt_updates->bindValue("id", $entries[$i]['id']);
        $stmt_updates->execute();
        $entries[$i]['updates'] = $stmt_updates->fetchAll();
    }
    return json_encode($entries);
});

$app->get('/years', function() use ($app) {
    $stmt = $app['db']->prepare("SELECT * FROM `year`;");
    $stmt->execute();
    $entries = $stmt->fetchAll();
    return json_encode($entries);
});

/**
 * Get Schools from database
 */
$app->get('/schools', function() use ($app) {
    $stmt = $app['db']->prepare("SELECT * FROM `schools`;");
    $stmt->execute();
    $entries = $stmt->fetchAll();
    return json_encode($entries);
});

$app->get('/schools/{id}', function($id) use ($app) {
    $stmt = $app['db']->prepare("SELECT * FROM `schools` WHERE `id` = :id");
    $stmt->bindValue("id", $id);
    $stmt->execute();
    $entry = $stmt->fetch();
    return json_encode($entry);
});

/**
 * Get cities from a specific School in database
 */
$app->get('/cities/school/{id_school}', function($id_school) use ($app) {
    $stmt = $app['db']->prepare('SELECT cities.* FROM `cities` JOIN `pivot_school_city` ON pivot_school_city.id_city = cities.id WHERE id_school = :id_school');
    $stmt->bindValue("id_school", $id_school);
    $stmt->execute();
    $entries = $stmt->fetchAll();
    return json_encode($entries);
});

/**
 * TODO: Update prepare query to join year / city / scool
 */
/*
$app->get('/groups/', function($id) use ($app) {
    $stmt = $app['db']->prepare('SELECT * FROM `groups` JOIN `group_logins` ON groups.id = group_logins.id_group ORDER BY name');
    $stmt->execute();
    $data = $stmt->fetchAll();
    $entries = [];
    foreach($data as $row) {
        if (!isset($entries[$row['id_school']])) {
            $entries[$row['id_school']] = [];
        }
        if (!isset($entries[$row['id_school']][$row['name']])) {
            $entries[$row['id_school']][$row['name']] = [];
        }
        array_push($entries[$row['id_school']][$row['name']], $row);
    }
    return json_encode($entries);
});
*/

$app->get('/groups/school/{id_school}/{id_city}', function($id_school, $id_city) use ($app) {
    $stmt = $app['db']->prepare('SELECT groups.* FROM `groups` JOIN pivot_school_city ON groups.id_pivot_school_city = pivot_school_city.id WHERE pivot_school_city.id_school = :id_school AND pivot_school_city.id_city = :id_city');
    $stmt->bindValue('id_school', $id_school);
    $stmt->bindValue('id_city', $id_city);
    $stmt->execute();
    $entries = $stmt->fetchAll();
    return new Response(json_encode($entries));
});

$app->get('/groups/school/{id_school}/{id_city}/{id_year}', function($id_school, $id_city, $id_year) use ($app) {
    $stmt = $app['db']->prepare('SELECT groups.* FROM `groups` JOIN pivot_school_city ON groups.id_pivot_school_city = pivot_school_city.id WHERE pivot_school_city.id_school = :id_school AND pivot_school_city.id_city = :id_city AND groups.id_year = :id_year');
    $stmt->bindValue('id_school', $id_school);
    $stmt->bindValue('id_city', $id_city);
    $stmt->bindValue('id_year', $id_year);
    $stmt->execute();
    $entries = $stmt->fetchAll();
    return new Response(json_encode($entries));
});

/**
 * Create a new group and add members
 * /!\ Warning /!\ No verification about ids
 */
$app->post('/groups', function(Request $request) use ($app) {
    $params = json_decode($request->getContent());
    $logins = explode(",", $params->logins);
    $id_pivot_school_city = $params->id_pivot_school_city;
    $id_year = $params->id_year;
    $name = $params->name;
    $stmt = $app['db']->prepare('SELECT id FROM `groups` WHERE id_pivot_school_city = :id_pivot_school_city AND name = :name AND id_year = :id_year');
    $stmt->execute(['id_pivot_school_city' => $id_pivot_school_city, 'name' => $name, 'id_year' => $id_year]);
    if (count($stmt->fetchAll()) > 0) {
        return new Response(json_encode(["message" => "Le Combo ecole / nom / année existe déjà"]), 422);
    }
    $stmt = $app['db']->prepare('INSERT INTO groups (id_pivot_school_city, id_year, name) VALUES(:id_pivot_school_city, :id_year, :name)');
    $stmt->execute(['id_pivot_school_city' => $id_pivot_school_city, 'name' => $name, 'id_year' => $id_year]);
    $id_group = $app['db']->lastInsertId();
    foreach($logins as $login) {
        $stmt = $app['db']->prepare('INSERT INTO `group_logins` (id_group, login) VALUES (:id_group, :login);');
        $stmt->execute(['id_group' => $id_group, 'login' => $login]);
    }
    return new Response(json_encode($logins), 201);
});

/*
$app->put('/groups/{id_school}/', function(Request $request) use($app) {
    $params = json_decode($request->getContent());
    $logins = explode(",", $params->logins);
});
*/

function getSchoolData($app, $school_id) {
    $stmt = $app['db']->prepare('SELECT * FROM schools WHERE id = :id');
    $stmt->execute(['id' => $school_id]);
    return $stmt->fetch();
}

$app->get('/groups/data/{id_group}', function($id_group) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $stmt = $app['db']->prepare('SELECT login, cities.slug as location, year.year as scholaryear, schools.slug as school, schools.course as course, CONCAT(schools.semester_code, schools.semester_number) as semester FROM group_logins JOIN groups ON group_logins.id_group = groups.id JOIN pivot_school_city ON pivot_school_city.id = groups.id_pivot_school_city JOIN year ON year.id = groups.id_year JOIN cities ON pivot_school_city.id_city = cities.id JOIN schools ON pivot_school_city.id_school = schools.id WHERE id_group = :id_group;');
    $stmt->execute(['id_group' => $id_group]);
    $res = $stmt->fetchAll();
    if (count($res) == 0) {
        return new Response(json_encode(["message => Impossible de trouver le groupe ou le groupe est vide."]), 404);
    }
    $students = getStudentsFromEpitech($res[0]['location'], $res[0]['scholaryear'], str_replace(" ", "%20", $res[0]['school']), $res[0]['semester'], $res[0]['course']);
    $data = [];
    foreach($students as $student) {
        $found = false;
        for($i = 0; $i < count($res); $i++) {
            if ($res[$i]['login'] == $student->login) {
                $found = true;
                break;
            }
        }
        if ($found) {
            $student->netsoul = json_decode(get_netsoul($wrapper, $student->login));
            $student->notes = json_decode(get_notes($wrapper, $student->login));
            $student->student = json_decode(get_student($wrapper, $student->login));
            $data[] = $student;
        }
    }
    return new Response(json_encode($data));
});

function getStudentsFromEpitech($location, $scolaryear, $school, $semester, $course) {
    $wrapper = new CurlWrapper($app['cookie']);
    //https://intra.epitech.eu/admin/promo/list?school=webacademie&scolaryear=2015&course=webacademie&semester=W1&location=FR/LYN&format=json
    $students = json_decode($wrapper->get(
        'https://intra.epitech.eu/admin/promo/list?' .
        '&location=' . $location .
        '&scolaryear=' . $scolaryear .
        '&school=' . $school .
        '&course=' . $course .
        '&semester=' . $semester .
        '&format=json'
    ));
    return $students;
}

$app->get('/students/{country}/{town}/{scolaryear}/{id_school}/{semester}', function ($country, $town, $scolaryear, $id_school, $semester) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $school_data = getSchoolData($app, $id_school);
    $students = getStudentsFromEpitech($country . '/' . $town, $scolaryear, $school_data['slug'], $semester, $school_data['course']);
    foreach ($students as $student) 
    {
        $student->netsoul = json_decode(get_netsoul($wrapper, $student->login));
        $student->notes = json_decode(get_notes($wrapper, $student->login));
        $student->student = json_decode(get_student($wrapper, $student->login));
    }
    return new Response(json_encode($students), 200);
});

function get_student ($wrapper, $login) {
    return $wrapper->get('https://intra.epitech.eu/user/' . $login . '/?format=json');
}

$app->get('/student/{login}', function ($login) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    return new Response(get_student($wrapper, $login), 200);
});

function get_netsoul($wrapper, $login) {
    $dates = json_decode($wrapper->get('https://intra.epitech.eu/user/' . $login . '/netsoul?format=json'));
    foreach ($dates as $key => &$date) {
        array_push($date, $date[0]);
        if (isset($date[0])) {
            $date[0] = date('Y-m-d', $date[0]);
        }
        if (isset($date[1]) && $date[1] !== 0) {
            $date[1] = ($date[1] / 60) / 60;
        }
    }
    usort($dates, 'sortByOrder');
    $jours_ouvres = 0;
    $jours_semaine = 0;
    $logtime_ouvres = 0;
    $logtime_semaine = 0;
    for ($i=count($dates)-2; $i >= 0; $i--) {
        $date = $dates[$i];
        $day = date('w', $date[count($date) - 1]);
        if ($jours_semaine < 7) {
            $logtime_semaine += $date[1];
            $jours_semaine++;
        }
        if ($jours_ouvres < 5 && $day != '6' && $day != '0') {
            $logtime_ouvres += $date[1];
            $jours_ouvres++;
        }
    }
    $logtime_semaine_avg = $logtime_semaine / 7;
    $logtime_ouvres_avg = $logtime_ouvres / 5;
    return array($logtime_semaine, $logtime_semaine_avg, $logtime_ouvres, $logtime_ouvres_avg, $dates);
}

$app->get('/netsoul/{login}', function ($login) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    return new Response(json_encode(get_netsoul($wrapper, $login)), 200);
});

$app->get('/modules/{country}/{town}/{scolaryear}/{id_school}/{semester}', function ($country, $town, $scolaryear, $id_school, $semester) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $school_data = getSchoolData($app, $id_school);
    $school = $school_data['slug'];
    $course = $school_data['course']; 
    $json_modules = json_decode($wrapper->get(
        'https://intra.epitech.eu/admin/module/list?' .
        '&location=' . $country  . '/' . $town .
        '&scolaryear=' . $scolaryear .
        '&school=' . $school .
        '&course=' . $course .
        '&semester=' . $semester .
        '&format=json'
    ));
    $json_students = getStudentsFromEpitech($country . '/' . $town, $scolaryear, $school, $semester, $course);
    $students = [];
    foreach ($json_students as $v)
    {
        $students[$v->login]['credits'] = $v->credits;
        $students[$v->login]['title'] = $v->title;
    }
    ksort($students);
    $modules = [];
    foreach ($json_modules as $k)
    {
        $instance = $k->code . $k->codeinstance;
        $modules[$instance]['title'] = $k->title;
        $modules[$instance]['code'] = $k->code;
        $modules[$instance]['credits'] = $k->credits;
        $modules[$instance]['codeinstance'][] = $k->codeinstance;
        sort($modules[$instance]['codeinstance']);
        $data = json_decode($wrapper->get('https://intra.epitech.eu/module/' . $scolaryear . '/' . $k->code . '/' . $k->codeinstance . '/notes?format=json'));
        unset($data->columns->login);
        $modules[$instance]['projects'] = [];
        foreach ($data->columns as $key => $val)
        {
            $modules[$instance]['projects'][$key] = $val->help;
        }
        foreach ($data->listnotes as $list)
        {
            if (array_key_exists($list->login, $students))
            {
                foreach ($modules[$instance]['projects'] as $key => $val)
                {
                    if (isset($list->{$key}))
                        $modules[$instance]['students'][$list->login][$key] = $list->{$key};
                }
            }
        }
    }
    return new Response(json_encode($modules), 200);
});

$app->get('/logs', function () use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $logs = $wrapper->get('https://intra.epitech.eu/pedago/log/get?format=json');
    return new Response($logs, 200);
});

$app->get('/pp/{country}/{town}/{scolaryear}/{semester}', function ($country = 'FR', $town = 'LYN', $scolaryear = 2015, $semester = 1) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $modulename = 'W-ADM-' . ($semester - 1) . '50';
    $location = $town . '-' . $semester . '-1';
    $query = 'https://intra.epitech.eu/module/' . $scolaryear . '/' . $modulename . '/' . $location;
    $module = json_decode($wrapper->get($query . '/?format=json'));
    $slots = [];
    if (isset($module->activites)) {
        $last_monday = date('Y-m-d', strtotime('last Monday'));
        $next_sunday = date('Y-m-d', strtotime('next Sunday'));
        foreach ($module->activites as $key => $cur_activity) {
            foreach ($cur_activity->events as $key => $cur_event) {
                $current_acti = substr($cur_activity->end, 0, 10);
                if ($current_acti >= $last_monday && $current_acti <= $next_sunday) {
                    $query_activity = $query . '/' . $cur_activity->codeacti . '/rdv';
                    $activity = json_decode($wrapper->get($query_activity . '/?format=json'));
                    if ($activity->title == 'Point Pangolin') {
                        foreach ($activity->slots as $key => $value) {
                            $slots = array_merge($slots, $value->slots);
                        }
                    }
                }
            }
        }
    }
    return new Response(json_encode($slots), 200);
});

function get_notes ($wrapper, $login) {
    return $wrapper->get('https://intra.epitech.eu/user/' . $login . '/notes?format=json');
}

$app->get('/notes/{login}', function ($login) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    return new Response(get_notes($wrapper, $login), 200);
});

$app->get('/suivis/{login}', function ($login) use ($app) {
    $wrapper = new CurlWrapper($app['cookie']);
    $suivis = $wrapper->get('https://intra.epitech.eu/user/' . $login . '/comments/list?format=json&offset=0&types[]=suivi_adm');
    return new Response($suivis, 200);
});

$app->post('/login', function (Request $request) use ($app) {
    $params = json_decode($request->getContent());
    if ($params === null) {
        $login = $request->query->get('login');
        $password = $request->query->get('password');
        $auth = $request->query->get('auth');
    } else {
        $login = $params->login;
        $password = $params->password;
        $auth = $params->auth;
    }
    session_regenerate_id();
    $app['cookie'] = realpath('cookies') . "/php_cookie_" . $login . ".txt";
    if ($auth == 'classic' || $auth == null) {
        require_once __DIR__.'/classic_connect.php';
        $http_code = classic_connect($app['cookie'], $login, $password);
    }
    else {
        require_once __DIR__.'/microsoft_connect.php';
        $http_code = microsoft_connect($app['cookie'], $login, $password);
    }
    if ($http_code == 200) {
        $wrapper = new CurlWrapper($app['cookie']);
        $profil = json_decode($wrapper->get(
            'https://intra.epitech.eu/user/' . $login . '/?format=json'
        ));
    	$app['session']->set('user', array('login' => $login, 'groups' => $profil->groups));
        $user = $app['session']->get('user');
        $admin = false;
        foreach ($user['groups'] as $key => $value) {
            if ($value->title == ADMIN_GROUP) {
                $admin = true;
            }
        }
        $sql = "SELECT * FROM users WHERE login = ?";
        $post = $app['db']->fetchAssoc($sql, array($login));
        if (count($post) === 0) {
            $app['db']->insert('users', array('login' => $login, 'admin' => $admin));
        } else {
            $app['db']->update('users', array('admin' => $admin), array('login' => $login));
        }
        return new Response('OK', 200);
    } else {
        $app['session']->set('user', null);
    }
    return new Response('KO', $http_code);
});

$app->get('/logout', function () use ($app) {
	if ($app['session']->get('user') != null && file_exists($app['cookie'])) {
		unlink($app['cookie']);
	}
    session_unset();
    return true;
});

$app->get('/isLogged', function () use ($app) {
    if ($app['session']->get('user') != null) {
        return 'OK';
    }
    return 'KO';
});

$app->run();