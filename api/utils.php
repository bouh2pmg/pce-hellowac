<?php

function getSchoolSemester($school, $semester) {
    if ($school == 'Samsung-WAC') {
        $semester = 'S' . $semester;
    } else {
        $semester = 'W' . $semester;
    }
    return $semester;
}

function getSchool($course) {
    if ($course == 'wac-ambition-feminine') {
        return 'webacademie';
    } else return $course;
}

function sortByOrder($a, $b) {
    return $a[0] > $b[0];
}

?>