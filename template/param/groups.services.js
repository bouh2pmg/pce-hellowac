function groupsService($http) {
    return {
        getGroupsFromSchoolCity: function(id_school, id_city) {
            console.log('api/index.php/groups/school/' + id_school + '/' + id_city);
            return $http({
                url: 'api/index.php/groups/school/' + id_school + '/' + id_city,
                method: "GET"
            });
        },
        getGroupsFromSchoolCityYear: function(id_school, id_city, id_year) {
            console.log('api/index.php/groups/school/' + id_school + '/' + id_city + '/' + id_year);
            return $http({
                url: 'api/index.php/groups/school/' + id_school + '/' + id_city + '/' + id_year,
                method: "GET"
            });
        },
        getGroupStudents: function(id) {
            console.log('api/index.php/groups/data/' + id);
            return $http({
                url: 'api/index.php/groups/data/' + id,
                method: "GET"
            });
        },
        getCity: function(id) {
            console.log('api/index.php/cities/school/' + id);
            return $http({
                url: 'api/cities/school/' + id,
                method: "GET"
            });
        },
        postGroup: function(logins, id_pivot, id_year, name) {
            // TODO
        }
    }
}
angular.module('app').service('Groups', groupsService);