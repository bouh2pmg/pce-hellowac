function ParamController($scope, $http, $location, $localStorage, Schools, Groups) {
    $scope.data = {
        school: "",
        id_school: "",
        school_slug: "",
        location: "",
        location_name: "",
        id_city: "",
        scolaryear: "",
        id_year: "",
        semester: "",
        group: "",
        id_group: ""
    };
    $scope.flushData = function() {
        $scope.data.group = "";
        $scope.data.id_group = "";
        $scope.scolaryearSelected = null;
        $scope.schoolSelected = null;
        $scope.citySelected = null;
        $scope.groupSelected = null;
    }
    $scope.school = null;
    $scope.semesters = [];
    $scope.groups = [];
    $scope.flushData();

    $scope.save = function() {
        Object.keys($scope.data).forEach(key => {
            $localStorage[key] = $scope.data[key];
        });
        $scope.flushData();
        $scope.reload($localStorage);
        $('#paramModal').modal('hide');
    }
    Schools.getSchools().success(function(schools) {
        $scope.schools = schools;
        $scope.cities = {};
        schools.forEach(function(school) {
            Schools.getCities(school.id).success(function(cities) {
                $scope.cities[school.id] = cities;
            });
        });
    });
    Schools.getYears().success(function(years) {
        $scope.years = years;
    })
    if ($scope.reload !== undefined) $scope.reload($localStorage);

    $scope.addSchool = function(school) {
        if (school != null) {
            $scope.data.school = school.name;
            $scope.data.id_school = school.id;
            $scope.school = school;
            $scope.semesters = getSemesters(school);
        }
    }

    $scope.addCity = function(city) {
        if (city != null) {
            $scope.data.location_name = city.name;
            $scope.data.id_city = city.id;
            $scope.data.location = city.slug;
            getGroups($scope.school.id, city.id);
        }
    }

    $scope.addYear = function(year) {
        if (year != null) {
            $scope.data.scolaryear = year.year;
            $scope.data.id_year = year.id;
        }
    }

    $scope.addGroup = function(group) {
        if (group != null) {
            $scope.data.group = group.name;
            $scope.data.id_group = group.id;
        }
    }

    function getGroups(id_school, id_city) {
        return Groups.getGroupsFromSchoolCity(id_school, id_city).success(function(groups) {
            $scope.groups = {};
            groups.forEach(function(group) {
                if ($scope.groups[group.id_year] == undefined) {
                    $scope.groups[group.id_year] = [group];
                } else {
                    $scope.groups[group.id_year].push(group);
                }
            });
        });
    }

    function getSemesters(school) {
        if (school != null) {
            let arr = [];
            for (i = 1; i <= school.semester_number; i++) {
                arr.push(school.semester_code + i);
            }
            return arr;
        }
        return [];
    }
}

angular.module('app').controller('ParamCtrl', ParamController);