function schoolsService($http) {
    return {
        getSchools: function() {
            console.log('api/index.php/schools');
            return $http({
                url: 'api/index.php/schools',
                method: "GET"
            });
        },
        getSchool: function(id) {
            console.log('api/index.php/schools/' + id);
            return $http({
                url: 'api/index.php/schools/' + id,
                method: "GET"
            });
        },
        getCities: function(id) {
            console.log('api/index.php/cities/school/' + id);
            return $http({
                url: 'api/index.php/cities/school/' + id,
                method: "GET"
            });
        },
        getYears: function() {
            console.log('api/index.php/years');
            return $http({
                url: 'api/index.php/years',
                method: "GET"
            });
        }
    }
}
angular.module('app').service('Schools', schoolsService);