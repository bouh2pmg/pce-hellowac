function HeaderController($scope, $http, $location) {
	$scope.search = function (login) {
		$location.url('/student/' + login);
	}
	$scope.logout = function () {
		$http({
			url: 'api/index.php/logout',
			method: "GET"
		});
		$location.url('/login');
	}
	$scope.url = $location.url();
	$scope.openModalParam = function () {
		$('#paramModal').modal('show');
	}
	$scope.openModalAgenda = function () {
		$('#agendaModal').modal('show');
		$.material.init();
	}
}

angular.module('app').controller('HeaderCtrl', HeaderController);