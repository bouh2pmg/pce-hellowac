function AgendaController($scope, $http, $location, $localStorage) {
	$scope.$storage = $localStorage.$default({
	    agenda: 'all',
	});
	$scope.save = function (options) {
		$scope.reload($scope.$storage);
		$('#agendaModal').modal('hide');
	}
	// $scope.reload($scope.$storage);
}

angular.module('app').controller('AgendaCtrl', AgendaController);
