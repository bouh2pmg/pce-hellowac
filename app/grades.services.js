function gradesService() {
	function set_color(node, note) {
		node.color = 'alert-danger';
		if (note <= 0) {
			node.color = 'bg-grey';
		} else if (note < 8) {
			node.color = 'alert-danger';
		} else if (note < 10) {
			node.color = 'alert-warning';
		} else if (note < 13) {
			node.color = 'alert-info';
		} else if (note < 15) {
			node.color = 'bg-success';
		} else if (note >= 15) {
			node.color = 'alert-success';
		}
		return node;
	}
	return {
		setColor: function (node, note) {
			return set_color(node, note);
		},
		setGrade: function(node, credits, params, attr) {
			var avg = null;
			if (typeof(node) === "undefined") {
				return;
			}
			if (node.length === 0) {
				return;
			}
			var count = 0;
			var note_max = 0;
			for (var i in node) {
				count++;
				var note = 0;
				if (typeof(attr) !== "undefined") {
					note = parseFloat(node[i][attr]);
				} else {
					note = parseFloat(node[i]);
					node[i] = {note: note};
				}
				if (note > note_max) {
					note_max = note;
				}
				if (avg === null) {
					avg = note;
				} else {
					avg += note;
				}
				set_color(node[i], note);
			}
			avg /= count;
			node.moyenne = {note: avg.toFixed(2)};
			set_color(node.moyenne, avg);
			node.credits = credits;
			var half = credits / 2;
			var unit = half / 4;
			node.grade = 'Echec';
			var d = 8;
			var c = 10;
			var b = 13;
			var a = 15;
			//if (typeof (params) !== 'undefined' &&
			//	typeof(params.school) !== "undefined" &&
			//	typeof (params.module) !== 'undefined' &&
			//	typeof (params.town) !== 'undefined' &&
			//	params.school == 'webacademie' &&
			//	params.town == 'FR/LYN') {
			//}
			if (typeof (params) !== 'undefined' &&
				typeof (params.module) !== 'undefined' &&
				params.module == 'W-WEB-024') {
				d = 6;
			}
			if (typeof (params) !== 'undefined' &&
				typeof (params.module) !== 'undefined' && (
					params.module == 'W-WEB-033' ||
					params.module == 'W-WEB-034'
				)
			) {
				avg = note_max;
			} else if (typeof (params) !== 'undefined' &&
				typeof (params.module) !== 'undefined' &&
				params.module == 'W-BDD-050'
			) {
				if (note_max == 20) {
					avg = a;
				} else if (note_max >= 16) {
					avg = b;
				} else if (note_max >= 13) {
					avg = c;
				} else if (note_max >= 10) {
					avg = d;
				}
			}
			if (avg < d);
			else if (avg < c) {
				node.grade = 'D';
				node.credits = Math.floor(half + unit);
			} else if (avg < b) {
				node.grade = 'C';
				node.credits = Math.floor(half + unit * 2);
			} else if (avg < a) {
				node.grade = 'B';
				node.credits = Math.floor(half + unit * 3);
			} else if (avg >= a) {
				node.grade = 'A';
				node.credits = credits;
			}
			if (credits == 1) {
				node.credits = 1;
			} else if (credits == 2) {
				if (node.grade == 'A' || node.grade == 'B') {
					node.credits = 2;
				} else if (node.grade == 'C' || node.grade == 'D') {
					node.credits = 1;
				}
			}
			//if (node.grade == 'Echec' && fourteen === true && valid_fourteen === true) {
			//	node.grade = 'D';
			//	node.credits = Math.round(half + unit);
			//}
			node.ghost = false;
			node.medal = false;
			node.remarkable = false;
			node.difficulty = false;
			if (avg <= 0) {
				node.ghost = true;
			}
			if (avg >= 16) {
				node.medal = true;
			}
			//if (sixteen == true) {
			//	node.remarkable = true;
			//}
			if (avg <= 5) {
				node.difficulty = true;
			}
			return node;
		}
	};
}
angular.module('app').service('Grades', gradesService);