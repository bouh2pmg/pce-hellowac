function authService() {
	var user = window.user;
	return {
		getUser: function() {
			return user;
		},
		setUser: function(newUser) {
			user = newUser;
		},
		isConnected: function() {
			return !!user;
		}
	};
}
angular.module('app').service('Auth', authService);