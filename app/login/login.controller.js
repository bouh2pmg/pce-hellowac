function LoginController($scope, $location, $http, Auth, $localStorage) {
	$scope.alerts = [];
	$scope.user = {};
	$scope.$storage = $localStorage.$default({
	    auth: "365"
	});
	$localStorage.login = '';
	$scope.submitLogin = function(user) {
		$http.post('api/index.php/login', {'login': user.login, 'password': user.password, 'auth': $scope.$storage.auth})
		.success(function(msg) {
			console.log(msg);
			if (msg == 'OK') {
				// Auth.setUser(user.login);
				$localStorage.login = user.login;
				// console.log($localStorage.login);
				$location.url('/');
			} else {
				$scope.addAlert('danger', 'Login ou mot de passe faux. Utilisez vos identifiants intranet.');
				$scope.loginError = msg;
			}
		})
		.error(function(data) {
			$scope.addAlert('warning', data);
			$scope.loginError = data;
		});
	};

	$scope.addAlert = function(type, msg) {
        $scope.alerts.push({type: type, msg: msg});
    };
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}

angular.module('app').controller('LoginCtrl', LoginController);