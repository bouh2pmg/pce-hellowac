function modulesService($http) {
    return {
        all: function(p) {
            console.log('api/index.php/modules/' + p['location'] + '/' + p['scolaryear'] + '/' + p['id_school'] + '/' + p['semester']);
            return $http({
                url: 'api/index.php/modules/' + p['location'] + '/' + p['scolaryear'] + '/' + p['id_school'] + '/' + p['semester'],
                method: "GET"
            });
        }
    }
}
angular.module('app').service('Modules', modulesService);