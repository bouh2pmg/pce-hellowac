function ModulesController($scope, $http, Modules, $location, $route, $routeParams, Grades, $timeout, $localStorage) {
	$scope.$storage = $localStorage;
	$scope.alerts = [];
	// $scope.orderObjectByProp = function () {
	// 	if (arguments.length < 2) {
	// 		return;
	// 	}
	// 	var obj = arguments[0];
	// 	var attr = "";
	// 	for (var i = 1; i < arguments.length; i++) {
	// 		attr += "['" + arguments[i] + "']";
	// 	};
	// 	var i = Object.keys(obj).length;
	// 	var swap = true;
	// 	while (i > 0 && swap) {
	// 		swap = false;
	// 		var old = false;
	// 		for (var j in obj) {
	// 			if (old) {
	// 				if (eval('obj[old]' + attr) > eval('obj[j]' + attr)) {
	// 					var tmp = obj[j];
	// 					obj[j] = obj[old];
	// 					obj[old] = tmp;
	// 					swap = true;
	// 				}
	// 			}
	// 			old = j;
	// 		}
	// 		i--;
	// 	}
	// 	console.log(obj);
	// }
	$scope.exportToCSV = function (code) {
		console.log(code);
		var module = $scope.modules[code];
		var csvContent = "";
		csvContent += "login,";
		csvContent += "grade,";
		csvContent += "credits,";
		csvContent += "difficulty,";
		csvContent += "medal,";
		csvContent += "ghost,";
		csvContent += "remarkable\n";
		for (student in module.students) {
			csvContent += module.students[student].login + ",";
			csvContent += module.students[student].grade + ",";
			csvContent += module.students[student].credits + ",";
			if (module.students[student].difficulty) csvContent += "1,"; else csvContent += "0,";
			if (module.students[student].medal) csvContent += "1,"; else csvContent += "0,";
			if (module.students[student].ghost) csvContent += "1,"; else csvContent += "0,";
			if (module.students[student].remarkable) csvContent += "1\n"; else csvContent += "0\n";
		}
		var blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
		var filename = code + ".csv";
		if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
	}
	$scope.reload = function (options) {
		$scope.modules = $localStorage['modules-' + options['location'] + '-' + options['scolaryear'] + '-' + options['school'] + '-' + options['semester']];
		$scope.alerts.push({type: 'info', msg: 'Loading des datas en cours.'});
		Modules.all(options).success(function (data) {
			data.all = {};
			for (var m in data) {
				var module = data[m];
				for (var s in module.students) {
					var student = module.students[s];
					var credits = module.credits;
					Grades.setGrade(student, module.credits, {module: data[m].code, school: $localStorage.school, town: $localStorage.location});
					student.login = s;
					if (typeof(data.all[s]) === "undefined") {
						data.all[s] = {};
					}
					data.all[s][m] = {};
					data.all[s][m].moyenne = student.moyenne;
					data.all[s][m].credits = parseInt(student.credits);
					data.all[s][m].grade = student.grade;
					data.all[s][m].orig_credits = parseInt(credits);
				}
			}
			for (var s in data.all) {
				var student = data.all[s];
				var avg = 0;
				var count = 0;
				var credits = 0;
				for (var m in student) {
					var module = student[m];
					avg += student[m].moyenne.note * student[m].orig_credits;
					count += student[m].orig_credits;
					if (student[m].grade != "Echec") {
						credits += parseInt(student[m].credits);
					}
				}
				avg = avg / count;
				student.moyenne = {note: avg.toFixed(2)};
				Grades.setColor(student.moyenne, avg);
				student.credits = credits;
				student.login = s;
			}
			if (Object.getOwnPropertyNames(data.all).length === 0) {
				$scope.alerts.push({type: 'danger', msg: 'Vous n\'avez pas les droits sur ce semestre. Selectionnez le module qui vous concerne dans les parametres.'});
			}
			$timeout(function () {
				$(".tablesorter").tablesorter();
			}, 0);
			$localStorage['modules-' + options['location'] + '-' + options['scolaryear'] + '-' + options['school'] + '-' + options['semester']] = data;
			$scope.modules = data;
			$scope.alerts.push({type: 'info', msg: 'Loading des datas terminé.'});
		});
	}
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
}

angular.module('app').controller('ModulesCtrl', ModulesController);