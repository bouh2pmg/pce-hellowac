var app = angular.module('app', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'ngStorage', 'ngAnimate']);

var checkIsConnected = function($q, $timeout, $http, $location) {
    var deferred = $q.defer();
    $http.get('api/index.php/isLogged').success(function(data) {
    	console.log(data);
        if (data == 'OK') {
            $timeout(deferred.resolve, 0);
        } else {
            $timeout(deferred.reject, 0);
            $location.url('/login');
        }
    });
    return deferred.promise;	
};



function Config($routeProvider) {
	$routeProvider
	.when('/login', {
		templateUrl: 'app/login/login.html',
		controller: 'LoginCtrl'
	})
	.when('/', {
		templateUrl: 'app/home/home.html',
		controller: 'HomeCtrl',
		resolve: {
			connected: checkIsConnected
		}
	})
	.when('/student/:login', {
		templateUrl: 'app/student/student.html',
		controller: 'StudentCtrl',
		resolve: {
			connected: checkIsConnected
		}
	})
	.when('/modules', {
		templateUrl: 'app/modules/modules.html',
		controller: 'ModulesCtrl',
		resolve: {
			connected: checkIsConnected
		}
	})
	.when('/yammer', {
		templateUrl: 'app/yammer/yammer.html',
		controller: 'YammerCtrl',
		resolve: {
			connected: checkIsConnected
		}
	})
	.otherwise({
		templateUrl: 'app/home/home.html',
		controller: 'HomeCtrl',
		resolve: {
			connected: checkIsConnected
		}
	});
}

app.config(['$routeProvider', Config]);