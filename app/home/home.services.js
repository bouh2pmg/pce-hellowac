function studentsService($http) {
    return {
        all: function(p) {
            if (p['id_group'] === null) {
                console.log('api/index.php/students/' + p['location'] + '/' + p['scolaryear'] + '/' + p['id_school'] + '/' + p['semester']);
                return $http({
                    url: 'api/index.php/students/' + p['location'] + '/' + p['scolaryear'] + '/' + p['id_school'] + '/' + p['semester'],
                    method: "GET"
                });
            } else {
                console.log('api/index.php/groups/data/' + p['id_group']);
                return $http({
                    url: 'api/index.php/groups/data/' + p['id_group'],
                    method: "GET"
                })
            }
        }
    }
}
angular.module('app').service('Students', studentsService);