function HomeController($scope, $http, Students, $location, $localStorage, Student, Logs) {
    $scope.alerts = [];
    $scope.storage = $localStorage;
    $scope.sortValue = 'login';
    $scope.refresh = function() {
        $scope.alerts.push({ type: 'info', msg: 'Loading des datas en cours.' });
        var options = {
            location: $localStorage['location'],
            scolaryear: $localStorage['scolaryear'],
            id_school: $localStorage['id_school'],
            semester: $localStorage['semester'],
            school: $localStorage['school'],
            id_group: $localStorage['id_group'] != undefined && $localStorage['id_group'].length > 0 ? $localStorage['id_group'] : null
        }
        Students.all(options).success(function(students) {
            Logs.all(options).success(function(logs) {
                for (var i = students.length - 1; i >= 0; i--) {
                    var login = students[i].login;
                    students[i].logs = 0;
                    for (var j = logs.length - 1; j >= 0; j--) {
                        if (logs[j].login == login) {
                            students[i].logs = logs[j].time / 60;
                            students[i].logs = students[i].logs.toFixed(1);
                        }
                    };
                };
            });
            Student.get_current_pp($scope.storage).success(function(data_pp) {
                for (var i = 0; i < students.length; i++) {
                    students[i].pp = false;
                    for (var j = 0; j < data_pp.length; j++) {
                        if (data_pp[j].master !== null) {
                            if (data_pp[j].master.login == students[i].login) {
                                students[i].pp = true;
                            }
                        }
                    }
                }
            });
            // for (var i = 0; i < students.length; i++) {
            // 	var student = students[i];
            // 	Student.get_comments(student.login).success(function (data) {
            // 		students[i].comments = data;
            // 	});
            // }
            $scope.students = students;
            options['group'] = options['group'] == undefined ? "" : options['group'];
            let name = 'students-' + options['location'] + '-' + options['scolaryear'] + '-' + options['school'] + '-' + options['semester'] + '-' + options['group'];
            $localStorage[name] = students;
            $scope.alerts.push({ type: 'info', msg: 'Loading des datas terminé.' });
        });
    }
    $scope.reload = function(options) {
        let name = 'students-' + options['location'] + '-' + options['scolaryear'] + '-' + options['school'] + '-' + options['semester'] + '-' + options['group'];
        var students = $localStorage[name];
        $scope.students = students;
    }
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}

angular.module('app').controller('HomeCtrl', HomeController);