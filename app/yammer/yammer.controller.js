function YammerController($scope, $http) {
	var interval = setInterval(function () {
		$("#embedded-feed").height(window.innerHeight - $(".navbar").height() + 40);
		$("#embedded-feed").css('margin-top', ($(".navbar").height() - 45) + 'px');
		console.log($("#header").height());
		if (typeof(yam) !== "undefined") {
			clearInterval(interval);
			yam.connect.embedFeed({
				container: "#embedded-feed",
				network: "webacademie",
				feedType: "group",
				feedId: "10055435"
			});
		}
	}, 100);
}

angular.module('app').controller('YammerCtrl', YammerController);