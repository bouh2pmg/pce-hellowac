function studentService($http) {
	return {
		get_modules: function(login) {
			console.log('api/index.php/notes/' + login);
			return $http({
				url: 'api/index.php/notes/' + login,
				method: "GET"
			});
		},
		get_suivi_adm: function (login) {
			console.log('api/index.php/suivis/' + login);
			return $http({
				url: 'api/index.php/suivis/' + login,
				method: "GET"
			});
		},
		get_current_pp: function (p) {
			console.log('api/index.php/pp/' + p['location'] + '/' + p['scolaryear'] + '/' + p['semester']);
			return $http({
				url: 'api/index.php/pp/' + p['location'] + '/' + p['scolaryear'] + '/' + p['semester'],
				method: "GET"
			});
		},
		get_situation: function (login) {
			console.log('api/index.php/student/' + login);
			return $http({
				url: 'api/index.php/student/' + login,
				method: "GET"
			});
		},
		add_comment: function (login, content, author) {
			console.log('api/index.php/comments/add', {
					login: login,
					content: content,
					author: author
				});
			return $http({
				url: 'api/index.php/comments/add',
				data: {
					login: login,
					content: content,
					author: author
				},
				method: "POST"
			});
		},
		update_comment_state: function (id, state) {
			console.log('api/index.php/comments/update/state/' + id + '/' + state);
			return $http({
				url: 'api/index.php/comments/update/state/' + id + '/' + state,
				method: "GET"
			});
		},
		update_comment_content: function (id, content, author) {
			console.log('api/index.php/comments/update/content/' + id);
			return $http({
				url: 'api/index.php/comments/update/content/' + id,
				data: {
					content: content,
					author: author
				},
				method: "POST"
			});
		},
		delete_comment: function (id) {
			console.log('api/index.php/comments/delete/' + id);
			return $http({
				url: 'api/index.php/comments/delete/' + id,
				method: "GET"
			});
		},
		get_comments: function (login) {
			console.log('api/index.php/comments/get/' + login);
			return $http({
				url: 'api/index.php/comments/get/' + login,
				method: "GET"
			});
		},
		get_tickets: function (login) {
			console.log('api/index.php/tickets/get/' + login);
			return $http({
				url: 'api/index.php/tickets/get/' + login,
				method: "GET"
			});
		}
	}
}
angular.module('app').service('Student', studentService);
