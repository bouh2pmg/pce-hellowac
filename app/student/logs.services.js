function logsService($http) {
	return {
		all: function(params) {
			console.log('api/index.php/logs');
			return $http({
				url: 'api/index.php/logs',
				method: "GET"
			});
		},
		netsoul: function (login) {
			console.log('api/index.php/netsoul/' + login);
			return $http({
				url: 'api/index.php/netsoul/' + login,
				method: "GET"
			});
		}
	}
}
angular.module('app').service('Logs', logsService);
