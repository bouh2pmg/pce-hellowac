function StudentController($scope, $http, Student, $location, $route, $routeParams, Grades, $timeout, Modules, $localStorage, Logs, $anchorScroll, Auth) {
	/**
	 * CHART.JS
	 */
	function generateChart(canvas, common, type) {
		if (common.labels.length === 0) {
			canvas.hide();
			return;
		}
		var datasets = [
			{
				label: "Moyenne de l'étudiant",
				fillColor : "#ff5722",
				strokeColor : "rgba(255,87,34, 0.9)",
				highlightFill: "rgba(255,87,34, 0.8)",
				highlightStroke: "rgba(255,87,34, 1)",
				data : common.data
			}
		];
		if (typeof(common.compare) !== "undefined") {
			datasets.push(
				{
					label: "Moyenne de la promotion",
					fillColor : "rgba(220,220,220,1)",
					strokeColor : "rgba(220,220,220,0.9)",
					highlightFill: "rgba(220,220,220,0.8)",
					highlightStroke: "rgba(220,220,220,1)",
					data : common.compare
				}
			);
		}
		var barChartData = {
			labels : common.labels,
			datasets : datasets
		}
		var ctx = canvas[0].getContext("2d");
		if (typeof(type) === "undefined" || type == 'bar') {
			window.myBar = new Chart(ctx).Bar(barChartData, {
				responsive : true,
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			});
		} else if (type == 'line') {
			window.myBar = new Chart(ctx).Line(barChartData, {
				responsive : true,
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			});
		}
	}
	function activateChart() {
		var labels_modules = [];
		var datas_modules = [];
		var compare_modules = [];
		var modules = $scope.modules;
		var promo_mods = $scope.promo;
		for (var m = 0; m < modules.length; m++) {
			var module = modules[m];
			if (module.notes.moyenne !== undefined) {
				labels_modules.push(module.title);
				datas_modules.push(module.notes.moyenne.note);
			} else {
				labels_modules.push(module.title);
				datas_modules.push(0);
			}
			if (typeof(promo_mods[module.codemodule + module.codeinstance]) !== "undefined") {
				compare_modules.push(promo_mods[module.codemodule + module.codeinstance].all.moyenne);
			} else {
				compare_modules.push(0);
			}
		}
		$timeout(function () {
			generateChart($('#canvasModules'), {labels: labels_modules, data: datas_modules, compare: compare_modules});
		}, 0);
		var labels_credits = [];
		var datas_credits = [];
		var compare_credits = [];
		for (var m = 0; m < modules.length; m++) {
			var module = modules[m];
			labels_credits.push(module.title);
			datas_credits.push(module.credits);
			if (typeof(promo_mods[module.codemodule + module.codeinstance]) !== "undefined") {
				compare_credits.push(promo_mods[module.codemodule + module.codeinstance].all.credits);
			} else {
				compare_credits.push(0);
			}
		}
		$timeout(function () {
			generateChart($('#canvasCredits'), {labels: labels_credits, data: datas_credits, compare: compare_credits});
		}, 0);
	}
	function activateChartNetsoul() {
		var data = $scope.netsoul;
		var commons_netsoul = {labels: [], data: []};
		for (var d in data) {
			var date = new Date(data[d][0]);
			commons_netsoul.labels.push(date.toString().substr(0, date.toString().length-24));
			commons_netsoul.data.push(data[d][1].toFixed(2));
		}
		commons_netsoul.labels = commons_netsoul.labels.slice(-50);
		commons_netsoul.data = commons_netsoul.data.slice(-50);
		$timeout(function () {
			generateChart($('#canvasNetsoul'), commons_netsoul, 'bar');
		}, 0);
		
	}
	$scope.radioModel = 'Notes';
	$scope.$watchCollection('radioModel', function () {
		if ($scope.radioModel == 'Graphs') {
			activateChart();
		}
		if ($scope.radioModel == 'Logs') {
			activateChartNetsoul();
		}
	});
	$scope.scrollTo = function(id) {
		$anchorScroll(id);
	}
	function calc_netsoul() {
		// $scope.netsoul_semaine = 0;
		// $scope.netsoul_cur_week = 0;
		// $scope.netsoul_mois = 0;
		$scope.absences = [];
		// var jour_ouvres = 0;
		// var cur_week_days = 0;
		var current_date = new Date();
		var current_month = current_date.getMonth();
		// var current_day_of_week = current_date.getDay();
		var current_year = parseInt(current_date.getUTCFullYear());
		var prec_month = current_month - 1;
		var prec_year = current_year;
		if (prec_month <= 0) {
			prec_month = 11;
			prec_year = current_year - 1;
		}
		// var nb_days_current_month = 0;
		// $scope.netsoul_cur_week_days = current_day_of_week - 1;
		for (var i = $scope.netsoul.length - 1; i >= 0; i--) {
			var node = $scope.netsoul[i];
			var date = new Date(node[0]);
			var day_of_week = date.getDay();
			var year = parseInt(date.getUTCFullYear());
			var month = date.getMonth();
			// if (day_of_week !== 0 && day_of_week != 6 && jour_ouvres < 5) {
			// 	$scope.netsoul_semaine += node[1];
			// 	jour_ouvres++;
			// 	if (day_of_week < current_day_of_week) {
			// 		$scope.netsoul_cur_week += node[1];
			// 	}
			// }
			// if (day_of_week !== 0 && day_of_week != 6 && current_month == month && year == current_year) {
			// 	$scope.netsoul_mois += node[1];
			// 	nb_days_current_month++;
			// }
			if (day_of_week !== 0 && day_of_week != 6 && ((current_month == month && year == current_year) || (prec_month == month && prec_year == current_year)) && node[1] <= $scope.log_min) {
				var date_to_string = new Date(node[0]);
				var date_to_string = date_to_string.toString().substr(0, date_to_string.toString().length-24);
				$scope.absences.push({
					date: date_to_string,
					dateOrd: node[0],
					time: node[1].toFixed(1)
				});
			}
		};
		// $scope.netsoul_mois /= nb_days_current_month;
		// $scope.netsoul_mois = $scope.netsoul_mois.toFixed(2);
		// $scope.netsoul_semaine  = $scope.netsoul_semaine.toFixed(2);
		// $scope.netsoul_cur_week = $scope.netsoul_cur_week.toFixed(2);
		// $scope.loaded.netsoul = true;
	}
	/**
	 * INIT
	 */
	// $scope.loaded = {suivi_adm: false, modules: false, all_modules: false, netsoul: false};
	$scope.alerts = [];
	$scope.showSuivis = true;
	$scope.showAbsences = true;
	$scope.showPP = true;
	$scope.showNotes = true;
	$scope.showModules = true;
	$scope.$storage = $;
	var param = $route.current.params.login;
	$scope.modules = Array();
	$scope.login = param;
	$scope.params = {};
	$scope.params.comment = '';
	$scope.params.log_min = 6;
	$scope.admin = false;
	$scope.selectLogs = function (item) {
		return item.time < $scope.params.log_min;
	}
	$scope.save_comment = function () {
		console.log($scope.params.comment)
		if ($scope.params.comment == '') {
			return false;
		}
		Student.add_comment(param, $scope.params.comment, $localStorage['login']).success(function (data) {
			$scope.alerts.push({type: 'success', msg: 'Note ajoutée au profil de l\'étudiant.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
		$scope.params.comment = '';
	}
		console.log(Auth.getUser());

	$scope.delete_comment = function (id) {
		Student.delete_comment(id).success(function (data) {
			$scope.alerts.push({type: 'danger', msg: 'Note supprimée du profil de l\'étudiant.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}

	$scope.update_comment_state = function (id, state) {
		Student.update_comment_state(id, state).success(function (data) {
			$scope.alerts.push({type: 'info', msg: 'Note mise à jour.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}
	$scope.update_comment = function (id, content) {
		console.log(id, content);
		Student.update_comment_content(id, content, $localStorage['login']).success(function (data) {
			$scope.alerts.push({type: 'info', msg: 'Note mise à jour.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}


	$scope.reload = function (options) {
		$scope.notes = $localStorage['student-' + param + '-notes'];
		$scope.modules = $localStorage['student-' + param + '-modules'];
		$scope.student = $localStorage['student-' + param + '-student'];
		$scope.situation = $localStorage['student-' + param + '-situation'];
		$scope.suivis = $localStorage['student-' + param + '-suivis'];
		$scope.absences = $localStorage['student-' + param + '-absences'];
		$scope.alerts.push({type: 'info', msg: 'Loading des datas en cours.'});
		Student.get_situation(param).success(function (data) {
			$scope.situation = data;
			$localStorage['student-' + param + '-situation'] = data;
		});
		Student.get_suivi_adm(param).success(function (data) {
			$scope.suivis = data;
			$localStorage['student-' + param + '-suivis'] = data;
			// $scope.loaded.suivi_adm = true;
		});
		Student.get_comments(param).success(function (data) {
			$scope.comments = data;
			$scope.admin = true;
		});
		Student.get_modules(param).success(function (data) {
			console.log(data);
			var modules = data.modules;
			modules.reverse();
			for (var module in modules) {
				modules[modules[module].codemodule] = modules[module];
				modules[module].notes = [];
			}
			var notes = data.notes;
			for (var note in notes) {
				if (typeof modules[notes[note].codemodule] !== "undefined") {
					modules[notes[note].codemodule].notes.push(notes[note]);
				}
			}
			var total = 0;
			for (var k = notes.length - 1; k >= 0; k--) {
				total += notes[k].final_note;
			};
			$scope.notes = data.notes;
			$scope.modules = data.modules;
			$scope.student = {};
			$scope.student.global_average = parseFloat((total/notes.length).toFixed(2));
			$scope.student.global_credits = 0;
			$scope.student.global_credits_total = 0;
			for (var i = modules.length - 1; i >= 0; i--) {
				modules[i].final_credits = modules[i].credits;
				Grades.setGrade(modules[i].notes, modules[i].credits, {module: modules[i].codemodule, school: $localStorage.school, town: $localStorage.location}, 'final_note');
				if (typeof(modules[i].notes.grade) !== "undefined") {
					$scope.student.global_credits_total += modules[i].credits;
					if (modules[i].notes.grade !== "Echec") {
						$scope.student.global_credits += modules[i].notes.credits;
					}
				}
			}
			$localStorage['student-' + param + '-notes'] = data.notes;
			$localStorage['student-' + param + '-modules'] = data.modules;
			$localStorage['student-' + param + '-student'] = data.student;
			$scope.alerts.push({type: 'info', msg: 'Loading des datas terminé.'});
			// $scope.loaded.modules = true;
		});
		// Modules.all(options).success(function (data) {
		// 	$scope.promo = data;
		// 	for (var m in data) {
		// 		var module = data[m];
		// 		var moyenne = 0;
		// 		var credits = 0;
		// 		var count = 0;
		// 		for (var s in module.students) {
		// 			var student = module.students[s];
		// 			Grades.setGrade(student, module.credits, {module: data[m].code, school: $localStorage.school, town: $localStorage.location});
		// 			moyenne += parseInt(student.moyenne.note);
		// 			credits += parseInt(student.credits);
		// 			count++;
		// 		}
		// 		module.all = {
		// 			moyenne: 0,
		// 			credits: 0
		// 		}
		// 		if (count > 0) {
		// 			module.all.moyenne = moyenne / count;
		// 			module.all.credits = credits / count;
		// 		}
		// 		Grades.setColor(module.all.moyenne, module.all.moyenne.note);
		// 	}
		// 	// $scope.loaded.all_modules = true;
		// });
		Logs.netsoul(param).success(function (data) {
			// $scope.netsoul = data[4];
			// $scope.netsoul_semaine = data[0].toFixed(2);
			// $scope.netsoul_semaine_avg = data[1].toFixed(2);
			// $scope.netsoul_ouvres = data[2].toFixed(2);
			// $scope.netsoul_ouvres_avg = data[3].toFixed(2);
			// calc_netsoul();
			// $localStorage['student-' + param + '-absences'] = $scope.absences;
			// $scope.$watchCollection('log_min', function () {
			// 	calc_netsoul();
			// });
		});
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
	};
}

angular.module('app').controller('StudentCtrl', StudentController);