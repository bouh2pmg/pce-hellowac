function StudentController($scope, $http, Student, $location, $route, $routeParams, Grades, $timeout, Modules, $localStorage, Logs, $anchorScroll, Auth) {
	$scope.scrollTo = function(id) {
		$anchorScroll(id);
	}
	/**
	 * INIT
	 */
	// $scope.loaded = {suivi_adm: false, modules: false, all_modules: false, netsoul: false};
	$scope.alerts = [];
	$scope.showSuivis = true;
	$scope.showAbsences = true;
	$scope.showPP = true;
	$scope.showNotes = true;
	$scope.showModules = true;
	$scope.$storage = $;
	var param = $route.current.params.login;
	$scope.modules = Array();
	$scope.login = param;
	$scope.params = {};
	$scope.params.comment = '';
	$scope.params.log_min = 6;
	$scope.admin = false;
	$scope.selectLogs = function (item) {
		return item.time < $scope.params.log_min;
	}
	$scope.save_comment = function () {
		console.log($scope.params.comment)
		if ($scope.params.comment == '') {
			return false;
		}
		Student.add_comment(param, $scope.params.comment, $localStorage['login']).success(function (data) {
			$scope.alerts.push({type: 'success', msg: 'Note ajoutée au profil de l\'étudiant.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
		$scope.params.comment = '';
	}
		console.log(Auth.getUser());

	$scope.delete_comment = function (id) {
		Student.delete_comment(id).success(function (data) {
			$scope.alerts.push({type: 'danger', msg: 'Note supprimée du profil de l\'étudiant.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}

	$scope.update_comment_state = function (id, state) {
		Student.update_comment_state(id, state).success(function (data) {
			$scope.alerts.push({type: 'info', msg: 'Note mise à jour.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}
	$scope.update_comment = function (id, content) {
		console.log(id, content);
		Student.update_comment_content(id, content, $localStorage['login']).success(function (data) {
			$scope.alerts.push({type: 'info', msg: 'Note mise à jour.'});
			Student.get_comments(param).success(function (data) {
				$scope.comments = data;
			});
		});
	}


	$scope.reload = function (options) {
		$scope.notes = $localStorage['student-' + param + '-notes'];
		$scope.modules = $localStorage['student-' + param + '-modules'];
		$scope.student = $localStorage['student-' + param + '-student'];
		$scope.situation = $localStorage['student-' + param + '-situation'];
		$scope.suivis = $localStorage['student-' + param + '-suivis'];
		$scope.absences = $localStorage['student-' + param + '-absences'];
		$scope.alerts.push({type: 'info', msg: 'Loading des datas en cours.'});
		Student.get_situation(param).success(function (data) {
			$scope.situation = data;
			$localStorage['student-' + param + '-situation'] = data;
		});
		Student.get_suivi_adm(param).success(function (data) {
			$scope.suivis = data;
			$localStorage['student-' + param + '-suivis'] = data;
			// $scope.loaded.suivi_adm = true;
		});
		Student.get_comments(param).success(function (data) {
			$scope.comments = data;
			$scope.admin = true;
		});
		Student.get_modules(param).success(function (data) {
			console.log(data);
			var modules = data.modules;
			modules.reverse();
			for (var module in modules) {
				modules[modules[module].codemodule] = modules[module];
				modules[module].notes = [];
			}
			var notes = data.notes;
			for (var note in notes) {
				if (typeof modules[notes[note].codemodule] !== "undefined") {
					modules[notes[note].codemodule].notes.push(notes[note]);
				}
			}
			var total = 0;
			for (var k = notes.length - 1; k >= 0; k--) {
				total += notes[k].final_note;
			};
			$scope.notes = data.notes;
			$scope.modules = data.modules;
			$scope.student = {};
			$scope.student.global_average = parseFloat((total/notes.length).toFixed(2));
			$scope.student.global_credits = 0;
			$scope.student.global_credits_total = 0;
			for (var i = modules.length - 1; i >= 0; i--) {
				modules[i].final_credits = modules[i].credits;
				Grades.setGrade(modules[i].notes, modules[i].credits, {module: modules[i].codemodule, school: $localStorage.school, town: $localStorage.location}, 'final_note');
				if (typeof(modules[i].notes.grade) !== "undefined") {
					$scope.student.global_credits_total += modules[i].credits;
					if (modules[i].notes.grade !== "Echec") {
						$scope.student.global_credits += modules[i].notes.credits;
					}
				}
			}
			$localStorage['student-' + param + '-notes'] = data.notes;
			$localStorage['student-' + param + '-modules'] = data.modules;
			$localStorage['student-' + param + '-student'] = data.student;
			$scope.alerts.push({type: 'info', msg: 'Loading des datas terminé.'});
			// $scope.loaded.modules = true;
		});
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
	};
}

angular.module('app').controller('StudentCtrl', StudentController);